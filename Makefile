all: obj/Pixel.o obj/Image.o obj/mainTest.o obj/mainExemple.o obj/mainAffichage.o bin/test bin/exemple bin/affichage doc

obj/Pixel.o: src/Pixel.cpp
	g++ -Wall -o obj/Pixel.o -c src/Pixel.cpp

obj/Image.o: src/Image.cpp
	g++ -Wall -o obj/Image.o -c src/Image.cpp

obj/mainTest.o: src/mainTest.cpp
	g++ -Wall -o obj/mainTest.o -c src/mainTest.cpp

obj/mainExemple.o: src/mainExemple.cpp
	g++ -Wall -o obj/mainExemple.o -c src/mainExemple.cpp

obj/mainAffichage.o: src/mainAffichage.cpp
	g++ -Wall -o obj/mainAffichage.o -c src/mainAffichage.cpp -lSDL2

bin/test: obj/mainTest.o
	g++ -Wall -o bin/test obj/mainTest.o

bin/exemple: obj/mainExemple.o
	g++ -Wall -o bin/exemple obj/mainExemple.o

bin/affichage: obj/mainAffichage.o
	g++ -Wall -o bin/affichage obj/mainAffichage.o -lSDL2

doc: doc/doxyfile
	doxygen doc/doxyfile

clean:
	rm -f obj/Pixel.o obj/Image.o obj/mainTest.o obj/mainExemple.o obj/mainAffichage.o bin/test bin/exemple bin/affichage doc