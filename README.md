Projet module d'image de HOANG Thai Ha p2104662

Pour compiler: make
Pour executer: bin/test bin/exemple bin/affichage

Ce module fournit des fonctionnalités pour afficher et manipuler des images à l'aide de SDL2.

test :
Cet exécutable est destiné à tester le module Image. Il effectue une série de tests unitaires pour vérifier que les fonctionnalités du module fonctionnent correctement. Les tests comprennent la création d'une image, la récupération et la modification de pixels, le dessin de rectangles, etc.

exemple :
Cet exécutable est un exemple d'utilisation du module Image. Il charge une image à partir du fichier spécifié en ligne de commande, effectue quelques manipulations (comme le dessin de rectangles) et sauvegarde l'image résultante dans un nouveau fichier.

affichage :
Cet exécutable est destiné à afficher une image à l'écran en utilisant SDL2. Il charge l'image spécifiée en ligne de commande et l'affiche dans une fenêtre graphique.

L'archive contient les fichiers suivants :
src/ : Répertoire contenant les sources du module
bin/ : Répertoire contenant les exécutables générés après compilation
obj/ : Ce répertoire est destiné à contenir les fichiers objets générés lors de la compilation des fichiers source.
data/ : Ce répertoire peut contenir des données nécessaires à l'exécution des programmes, telles que des images à charger ou à afficher.
doc/ : Répertoire contenant les contenus concernant doxygen
Makefile : Ce fichier contient les règles de compilation pour générer les exécutables à partir des fichiers sources.
README.md : Ce fichier expliquant l'utilisation du module