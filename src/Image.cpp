#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include "Image.h"
#include "Pixel.h"
#include "Pixel.cpp"
using namespace std;

// Constructeur par défaut
Image::Image () {
    tab = nullptr; 
    dimx = 0; 
    dimy = 0; 
}

// Constructeur avec dimensions spécifiées
Image::Image(int dimensionX, int dimensionY) : dimx(dimensionX), dimy(dimensionY) {
    assert(dimx > 0 && dimy > 0);
    tab = new Pixel[dimx * dimy];
}

// Destructeur
Image::~Image() {
    delete[] tab;
    dimx = dimy = 0;
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
Pixel& Image::getPix(unsigned int x,unsigned int y) {
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    return tab[y * dimx + x];
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
const Pixel& Image::getPix(unsigned int x,unsigned int y) const {
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    return tab[y * dimx + x];
}

// Modifie le pixel de coordonnées (x,y)
void Image::setPix(unsigned int x,unsigned int y, Pixel couleur) {
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    tab[y * dimx + x] = couleur;
}

// Dessine un rectangle plein de la couleur dans l'image
void Image::dessinerRectangle(unsigned int Xmin,unsigned int Ymin,unsigned int Xmax,unsigned int Ymax, Pixel couleur) {
    assert(Xmin >= 0 && Xmin < dimx && Ymin >= 0 && Ymin < dimy &&
           Xmax >= 0 && Xmax < dimx && Ymax >= 0 && Ymax < dimy);
    for (int unsigned i = Xmin; i <= Xmax; ++i) {
        for (int unsigned j = Ymin; j <= Ymax; ++j) {
            setPix(i, j, couleur);
        }
    }
}

// Efface l'image en la remplissant de la couleur en paramètre
void Image::effacer(Pixel couleur) {
    dessinerRectangle(0, 0, dimx - 1, dimy - 1, couleur);
}

// Effectue une série de tests vérifiant que toutes les fonctions fonctionnent correctement
void Image::testRegression() {
    // Test du constructeur par défaut
    Image img1;
    assert(img1.dimx == 0 && img1.dimy == 0 && img1.tab == nullptr);

    // Test du constructeur avec dimensions spécifiées
    Image img2(10, 10);
    assert(img2.dimx == 10 && img2.dimy == 10 && img2.tab != nullptr);

    // Test de la fonction getPix
    Pixel testPixel(100, 150, 200);
    img2.setPix(5, 5, testPixel);
    Pixel retrievedPixel = img2.getPix(5, 5);
    assert(retrievedPixel.r == testPixel.r && retrievedPixel.g == testPixel.g && retrievedPixel.b == testPixel.b);

    // Test de la fonction setPix
    Pixel newPixel(50, 75, 100);
    img2.setPix(5, 5, newPixel);
    retrievedPixel = img2.getPix(5, 5);
    assert(retrievedPixel.r == newPixel.r && retrievedPixel.g == newPixel.g && retrievedPixel.b == newPixel.b);

    // Test de la fonction dessinerRectangle
    Pixel rectColor(255, 0, 0);
    img2.dessinerRectangle(2, 2, 6, 6, rectColor);
    for (int unsigned i = 2; i <= 6; ++i) {
        for (int unsigned j = 2; j <= 6; ++j) {
            retrievedPixel = img2.getPix(i, j);
            assert(retrievedPixel.r == rectColor.r && retrievedPixel.g == rectColor.g && retrievedPixel.b == rectColor.b);
        }
    }

    // Test de la fonction effacer
    Pixel clearColor(0, 0, 0);
    img2.effacer(clearColor);
    for (int unsigned i = 0; i < img2.dimx; ++i) {
        for (int unsigned j = 0; j < img2.dimy; ++j) {
            retrievedPixel = img2.getPix(i, j);
            assert(retrievedPixel.r == clearColor.r && retrievedPixel.g == clearColor.g && retrievedPixel.b == clearColor.b);
        }
    }
}

// Sauver image dans un fichier donne
void Image::sauver(const std::string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

// Ouvrir un fichier donne
void Image::ouvrir(const std::string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    string r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> b >> g;
            Pixel pix;
            pix.r = stoi(r); 
            pix.g = stoi(g);
            pix.b = stoi(b);
            setPix(x, y, pix);
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

// Afficher console
void Image::afficherConsole()
{
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}
// get DimX de l'image
int Image::getDimx() const { return dimx; }
// get DimY de l'image
int Image::getDimy() const { return dimy; }