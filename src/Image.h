#ifndef _IMAGE_H
#define _IMAGE_H
#include "Pixel.h"
#include <string>
using namespace std;

class Image {
private:
    Pixel *tab; // le tableau 1D de pixel
    unsigned int dimx, dimy;  // les dimensions de l'image
public:
 // Constructeur de la classe : initialise dimx et dimy à 0
 // n’alloue aucune mémoire pour le tableau de pixel
    Image ();
 // Constructeur de la classe : initialise dimx et dimy (après vérification)
 // puis alloue le tableau de pixel dans le tas (image noire)
    Image (int dimensionX, int dimensionY);
 // Destructeur de la classe : déallocation de la mémoire du tableau de pixels
 // et mise à jour des champs dimx et dimy à 0
    ~Image ();
 // Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
 // La formule pour passer d'un tab 2D à un tab 1D est tab[y*dimx+x]
    Pixel& getPix (unsigned int x,unsigned int y);
 // Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
    const Pixel& getPix(unsigned int x,unsigned int y) const;

 // Modifie le pixel de coordonnées (x,y)
    void setPix (unsigned int x,unsigned int y, Pixel couleur);
 // Dessine un rectangle plein de la couleur dans l'image
 // (en utilisant setPix, indices en paramètre compris)
    void dessinerRectangle(unsigned int Xmin,unsigned int Ymin,unsigned int Xmax,unsigned int Ymax, Pixel couleur);
 // Efface l'image en la remplissant de la couleur en paramètre
 // (en appelant dessinerRectangle avec le bon rectangle)
    void effacer(Pixel couleur);
 // Effectue une série de tests vérifiant que toutes les fonctions fonctionnent et
 // font bien ce qu’elles sont censées faire, ainsi que les données membres de
 // l'objet sont conformes
    static void testRegression();
    // Sauver image dans un fichier donne
    void sauver(const std::string &filename) const;
    // Ouvrir un fichier donne
    void ouvrir(const std::string &filename);
    // Afficher console
    void afficherConsole();
    // get DimX de l'image
    int getDimx() const;
    // get DimY de l'image
    int getDimy() const;
};


#endif