#include "ImageViewer.h"
#include <SDL2/SDL.h>

ImageViewer::ImageViewer() : window(nullptr), renderer(nullptr) {
    // Initialisation de SDL2
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Erreur lors de l'initialisation de SDL2: " << SDL_GetError() << std::endl;
        // Gérer l'erreur appropriée ici
    }

    // Création de la fenêtre SDL
    window = SDL_CreateWindow("Image Viewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        std::cerr << "Erreur lors de la création de la fenêtre SDL: " << SDL_GetError() << std::endl;
        // Gérer l'erreur appropriée ici
    }

    // Création du renderer SDL
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr) {
        std::cerr << "Erreur lors de la création du renderer SDL: " << SDL_GetError() << std::endl;
        // Gérer l'erreur appropriée ici
    }
}

ImageViewer::~ImageViewer() {
    // Destruction du renderer
    if (renderer != nullptr) {
        SDL_DestroyRenderer(renderer);
        renderer = nullptr;
    }

    // Destruction de la fenêtre
    if (window != nullptr) {
        SDL_DestroyWindow(window);
        window = nullptr;
    }

    // Fermeture de SDL2
    SDL_Quit();
}

void ImageViewer::afficher(const Image& im) {
    // Vérifier que la fenêtre et le renderer sont valides
    if (window == nullptr || renderer == nullptr) {
        std::cerr << "La fenêtre ou le renderer SDL n'est pas initialisé." << std::endl;
        return;
    }

    // Vérifier que les dimensions de l'image sont positives
    if (im.getDimx() <= 0 || im.getDimy() <= 0) {
        std::cerr << "Les dimensions de l'image ne sont pas valides." << std::endl;
        return;
    }

    // Convertir le tableau de pixels de l'image en une SDL_Surface
    SDL_Surface* surface = SDL_CreateRGBSurfaceFrom(const_cast<Pixel*>(&im.getPix(0, 0)), im.getDimx(), im.getDimy(), 24, im.getDimx() * 3, 0xFF0000, 0x00FF00, 0x0000FF, 0);

    if (surface == nullptr) {
        std::cerr << "Erreur lors de la création de la SDL_Surface: " << SDL_GetError() << std::endl;
        return;
    }

    // Créer la SDL_Texture à partir de la SDL_Surface
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface); // La SDL_Texture a sa propre copie des données de la SDL_Surface, donc nous pouvons la libérer ici

    if (texture == nullptr) {
        std::cerr << "Erreur lors de la création de la SDL_Texture: " << SDL_GetError() << std::endl;
        return;
    }

    // Effacer l'écran avec une couleur de fond
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Fond blanc
    SDL_RenderClear(renderer);

    // Dessiner la texture sur le renderer
    SDL_RenderCopy(renderer, texture, nullptr, nullptr);

    // Rafraîchir l'affichage
    SDL_RenderPresent(renderer);

    // Attendre un certain temps avant de fermer la fenêtre (facultatif)
    SDL_Delay(2000); // Attendre 2 secondes

    // Libérer la texture
    SDL_DestroyTexture(texture);
}

