#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include "Image.h"
#include <SDL2/SDL.h>

class ImageViewer {
private:
    SDL_Window* window;
    SDL_Renderer* renderer;
    // Autres données SDL2 nécessaires, comme SDL_Surface et SDL_Texture

public:
    // Constructeur qui initialise tout SDL2 et crée la fenêtre et le renderer
    ImageViewer();

    // Destructeur qui détruit et ferme SDL2
    ~ImageViewer();

    // Affiche l'image passée en paramètre et permet le (dé)zoom
    void afficher(const Image& im);
};

#endif
