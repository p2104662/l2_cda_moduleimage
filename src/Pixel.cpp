#include <iostream>
#include "Pixel.h"
using namespace std;

// Constructeur par défaut de la classe: initialise le pixel à la couleur noire
Pixel::Pixel () {
    r = 0;
    g = 0;
    b = 0;
}

// Constructeur de la classe: initialise r,g,b avec les paramètres
Pixel::Pixel (unsigned int nr, unsigned int ng, unsigned int nb) {
    r = nr;
    g = ng;
    b = nb;
}
