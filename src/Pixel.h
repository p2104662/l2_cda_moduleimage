#ifndef _PIXEL_H
#define _PIXEL_H

struct Pixel {
 unsigned int r, g, b; // les composantes du pixel, unsigned char en C++
 // Constructeur par défaut de la classe: initialise le pixel à la couleur noire
 Pixel ();
 // Constructeur de la classe: initialise r,g,b avec les paramètres
 Pixel (unsigned int nr, unsigned int ng, unsigned int nb);
};

#endif
